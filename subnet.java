import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class subnet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter IP address (e.g., 192.168.1.1): ");
        String ipAddress = scanner.nextLine();

        System.out.print("Enter Subnet Mask (e.g., 24): ");
        int subnetMaskLength = scanner.nextInt();

        try {
            InetAddress inetAddress = InetAddress.getByName(ipAddress);
            String addressBytes = getNetworkClass(inetAddress.getAddress()[0]);
            String firstOctetRange = "";
            if ("C".equals(addressBytes)) {
                firstOctetRange = "192-223";
            } else if ("B".equals(addressBytes)) {
                firstOctetRange = "128-191";
            } else if ("A".equals(addressBytes)) {
                firstOctetRange = "1-126";
            } else {
                firstOctetRange = "Unknown";
            }

            String hexIpAddress = convertToHex(inetAddress.getAddress());
            String subnetMask = SubnetMaskBytes(subnetMaskLength);
            String wildcardMask = calculateWildcardMask(subnetMask);
            int subnetBits = subnetMaskLength;
            int maskBits = 32 - subnetMaskLength;
            int maximumSubnets = (int) Math.pow(2, maskBits);
            int hostsPerSubnet = (int) Math.pow(2, maskBits) - 2;
            String subnetId = NetworkAddressCal(inetAddress, subnetMaskLength).getHostAddress();
            String broadcastAddress = getBroadcastAddress(inetAddress, subnetMaskLength).getHostAddress();

            System.out.println("Network Class : " + addressBytes);
            System.out.println("IP Address : " + inetAddress.getHostAddress());
            System.out.println("First Octet Range : " + firstOctetRange);
            System.out.println("Hex IP Address : " + hexIpAddress);
            System.out.println("Subnet Mask : " + subnetMask);
            System.out.println("Wildcard Mask : " + wildcardMask);
            System.out.println("Subnet Bits : " + subnetBits);
            System.out.println("Mask Bits : " + maskBits);
            System.out.println("Maximun Subnets : " + maximumSubnets);
            System.out.println("Hosts per Subnet : " + hostsPerSubnet);
            System.out.println("Subnet ID: " + subnetId);
            System.out.println("Broadcast Address : " + broadcastAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        scanner.close();
    }

    public static String getNetworkClass(byte ipAddress) {

        if ((ipAddress & 0xE0) == 0xC0) {
            return "C";
        } else if ((ipAddress & 0xC0) == 0x80) {
            return "B";
        } else if ((ipAddress & 0x80) == 0) {
            return "A";
        } else if ((ipAddress & 0xF0) == 0xE0) {
            return "D";
        } else {
            return "E";
        }
    }

    private static String convertToHex(byte[] address) {
        StringBuilder hexIP = new StringBuilder();
        for (byte octet : address) {
            hexIP.append(String.format("%02X", octet));
        }
        return hexIP.toString();
    }

    private static String SubnetMaskBytes(int subnetMaskLength) {
        int subnetMask = 0xFFFFFFFF << (32 - subnetMaskLength);
        byte[] subnetMaskBytes = {
                (byte) (subnetMask >> 24 & 0xFF),
                (byte) (subnetMask >> 16 & 0xFF),
                (byte) (subnetMask >> 8 & 0xFF),
                (byte) (subnetMask & 0xFF)
        };
        try {
            return InetAddress.getByAddress(subnetMaskBytes).getHostAddress();
        } catch (UnknownHostException e) {
            return null;
        }
    }

    private static String calculateWildcardMask(String subnetMask) {
        try {
            String[] parts = subnetMask.split("\\.");
            if (parts.length != 4) {
                return "Invalid Subnet Mask";
            }

            StringBuilder wildcardMask = new StringBuilder();
            for (String part : parts) {
                int subnetOctet = Integer.parseInt(part);
                int wildcardOctet = 255 - subnetOctet;
                wildcardMask.append(wildcardOctet).append('.');
            }

            return wildcardMask.substring(0, wildcardMask.length() - 1);
        } catch (NumberFormatException e) {
            return "Invalid Subnet Mask";
        }
    }

    public static InetAddress NetworkAddressCal(InetAddress inetAddress, int subnetMaskLength)
            throws UnknownHostException {
        byte[] address = inetAddress.getAddress();
        for (int i = subnetMaskLength; i < address.length * 8; i++) {
            address[i / 8] &= ~(1 << (7 - i % 8));
        }
        return InetAddress.getByAddress(address);
    }

    private static InetAddress getBroadcastAddress(InetAddress inetAddress, int prefixLength)
            throws UnknownHostException {
        byte[] address = inetAddress.getAddress();
        for (int i = prefixLength; i < address.length * 8; i++) {
            address[i / 8] |= (1 << (7 - i % 8));
        }
        return InetAddress.getByAddress(address);
    }
}
